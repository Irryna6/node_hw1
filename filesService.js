// requires...
const fs = require('fs');
const path = require('path');

// const fileExtensions = [".log", ".txt", ".json", ".yaml", ".xml", ".js"];


function createFile (req, res, next) {
  if (!req.body.filename || !req.body.content) {
    res.status(400).send({"message": "Specify 'content' parameter"});
  }
 
  fs.writeFile(path.resolve(__dirname, 'files', req.body.filename), req.body.content, function (err) {
    if (err) {
      res.status(500).send({'message': 'Server error'});
    } else {
      res.status(200).send({"message": "File created successfully"});
    }

  })
}

function getFiles (req, res, next) {
  let filesList = fs.readdirSync(__dirname+'/files')
  if (filesList.length == 0){
    res.status(400).send({"warning": "Please add some files"});
  }
  else {
    res.status(200).send({
      "message": "Success",
      "files": filesList})
    } 
}

const getFile = (req, res, next) => {
  let filesList = fs.readdirSync(__dirname+'/files')
  let file_name = req.params.filename;
  if (filesList.includes(file_name)) {
    fs.readFile(__dirname + '/files' + '/' + file_name, 'utf-8', function (err, data) {
      if (err) {
        console.error(err)}
      else {
        console.log(data);
        res.status(200).send({
          "message": "Success",
          "filename": file_name,
          "content": data,
          "extension": path.extname(file_name).replace('.', ''),
          "uploadedDate": "2017-07-22T17:32:28Z"
        });
      }
  })
} 
  else {
    res.status(400).send({
      "message": `No file with name ${file_name}`})
  }
}


  
 

// // function deleteFile(req, res) {
// //   fs.remove('/files/file.txt', err => {
// //     if (err) return console.error(err)
// //     console.log('success!')
// //   })
// // }

// // // Other functions - editFile, deleteFile

// // // path.extName('file.txt') ---> '.txt'
// // // fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile,

}





